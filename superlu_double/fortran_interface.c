/* sparseSET - Nov 2020                                         */
/* This file contains the function to call superLU from fortran */
/* It was written to call superLU in sparseSET; All files from  */
/* superLU are released according to the BSD license.           */
/* This file, and sparse_set.f90 follow the same license (BSD)  */

#include "slu_ddefs.h"

void symRowColVal_to_genCompCol(int*, int*, int*, int*,
     double*, int**, int**, double**);

void superlu_solver_(int* n, int* nnz, int* rows, int* cols, double* vals,
                     double* vector, double* solution){
    /* n - Number of unknowns in the system of equation         */
    /* nnz - Number of non-nulls terms in the matrix            */
    /* rows - vector containing the row number of the term      */
    /* cols - vector containing the column number of the term   */
    /* vals - vector containing the value of the term           */
    /* vector - vector (right hand side)                        */
    /* solution - vector in which the solution is returned      */ 

    /* Arrays to assemble the supermatrix */
    int* colptr;
    int* rowind;
    double* nzval;
    symRowColVal_to_genCompCol(n, nnz, rows, cols, vals, 
    &rowind, &colptr, &nzval);

    SuperMatrix A, L, U, B;
    int      *perm_r; /* row permutations from partial pivoting */
    int      *perm_c; /* column permutation vector */
    superlu_options_t options;
    SuperLUStat_t stat;


    /* Creating the A matrix to be used in SuperLU */
    dCreate_CompCol_Matrix(&A, *n, *n, colptr[*n], nzval, rowind, colptr, 
    SLU_NC, SLU_D, SLU_GE);

    double* b = malloc(*n*sizeof(*vector));
    
    for (int i = 0; i < *n; i++) b[i] = vector[i];

    /* Creating the b vector to be used in SuperLU */
    dCreate_Dense_Matrix(&B, *n, 1, b, *n, SLU_DN, SLU_D, SLU_GE);
    
    if ( !(perm_r = intMalloc(*n)) ) ABORT("Malloc fails for perm_r[].");
    if ( !(perm_c = intMalloc(*n)) ) ABORT("Malloc fails for perm_c[].");
    
   /* Now we modify the default options to use the symmetric mode. */
    options.Fact = DOFACT;
    options.SymmetricMode = YES;
    options.ColPerm = MMD_AT_PLUS_A;
    options.DiagPivotThresh = 0.001;

    /* Initialize the statistics variables. */
    StatInit(&stat);

    int info;
    /* Solve the linear system. */
    dgssv(&options, &A, perm_c, perm_r, &L, &U, &B, &stat, &info);

    /*StatPrint(&stat);*/

    for (int i = 0; i < *n; i++)
        solution[i] = b[i];
    
    SUPERLU_FREE (perm_r);
    SUPERLU_FREE (perm_c);
    
    Destroy_CompCol_Matrix(&A);
    Destroy_SuperMatrix_Store(&B);
    Destroy_SuperNode_Matrix(&L);
    Destroy_CompCol_Matrix(&U);
    StatFree(&stat);
    
}

void assemble_and_print_(int* n, int* nnz, int* rows, int* cols, double* vals){
    /* n - Number of unknowns in the system of equation         */
    /* nnz - Number of non-nulls terms in the matrix            */
    /* rows - vector containing the row number of the term      */
    /* cols - vector containing the column number of the term   */
    /* vals - vector containing the value of the term           */

    /* This function was written only to test symRowColVal_to_genCompCol */

    
    int* colptr;
    int* rowind;
    double* nzval;
    symRowColVal_to_genCompCol(n, nnz, rows, cols, vals, 
    &rowind, &colptr, &nzval);
    printf("dsadsaa\n");
    for (int i = 0; i <= *n; i++)
        printf(" # %d -> colptr = %d\n",i,colptr[i]);


    for (int i = 0; i < colptr[*n]; i++)
        printf("# %d -> row %d, value = %lf\n",i,rowind[i],nzval[i]);

    free(colptr);
    free(rowind);
    free(nzval);

}

void symRowColVal_to_genCompCol(int* n, int* nnz, int* rows, int* cols,
     double* vals, int** rowind, int** colptr, double** nzval){
    /* Transform symmetric row/col/val matrix (sparseSET) to general */
    /* compressed column, to be used in superLU.                     */
    int* ntc; /* Number of terms in the column */
    ntc = malloc(*n*sizeof(*ntc));
    for (int i = 0; i < *n; i++) ntc[i] = 0;
    /* Counting terms in each column */
    for (int i = 0; i < *nnz; i++){
        ntc[rows[i]-1]++;
        if (rows[i] != cols[i]) ntc[cols[i]-1]++;
    }
    *colptr = (int*) malloc((*n+1)*sizeof(**colptr));
    int* count; /* count number of terms already added in the column */
    count = malloc(*n*sizeof(*count));
    for (int i = 0; i < *n; i++) count[i] = 0;
    (*colptr)[0] = 0;
    for (int i = 0; i < *n ; i++)
        (*colptr)[i+1] = (*colptr)[i] + ntc[i];
    int nnz_ = (*colptr)[*n];

    free(ntc);
    /* Allocating rowind and nzval */
    *rowind = (int*) malloc(nnz_*sizeof(**rowind));
    *nzval = (double*) malloc(nnz_*sizeof(**nzval));
    /* Filling rowind and nzval */
    int ind_;
    for (int i = 0; i < *nnz; i++){
        ind_ = (*colptr)[rows[i]-1] + count[rows[i]-1];
        (*rowind)[ind_] = cols[i]-1;
        (*nzval)[ind_] = vals[i];
        count[rows[i]-1]++;
        if (rows[i] != cols[i]){
            ind_ = (*colptr)[cols[i]-1] + count[cols[i]-1];
            (*rowind)[ind_] = rows[i]-1;
            (*nzval)[ind_] = vals[i];
            count[cols[i]-1]++;
        }
    }

}
