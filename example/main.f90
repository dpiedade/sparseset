program main
    use truss
    implicit none
    character(60)::filename

    write(*,'(a)',advance='no')"Input file name:"
    read(*,*)filename
    call read_input(filename)
    !call print_data()
    call assemble_system_of_equation()
    call solve_system_of_equation_()
    call compute_normal_forces()
    !call print_results()

end program main
