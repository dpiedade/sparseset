program test_sparse
    use sparse
    implicit none

    type(sparse_matrix)::spmat
    real(8),dimension(6)::b,x,diff,Ax

    integer::i

    call prepare_to_use(spmat,6,13)

    ! Here we only want to test the method to transform the sparseSET matrix
    ! to be sent to superLU

    ! First row
    spmat%row(1) = 1; spmat%col(1) = 1; spmat%val(1) = 110.0d0
    spmat%row(2) = 1; spmat%col(2) = 4; spmat%val(2) = 14.0d0
    spmat%row(3) = 1; spmat%col(3) = 5; spmat%val(3) = 15.0d0
    spmat%row(4) = 1; spmat%col(4) = 6; spmat%val(4) = 16.0d0    
    ! Second row
    spmat%row(5) = 2; spmat%col(5) = 2; spmat%val(5) = 220.0d0
    spmat%row(6) = 2; spmat%col(6) = 4; spmat%val(6) = 24.0d0
    spmat%row(7) = 2; spmat%col(7) = 5; spmat%val(7) = 25.0d0
    ! Third row
    spmat%row(8) = 3; spmat%col(8) = 3; spmat%val(8) = 330.0d0
    spmat%row(9) = 3; spmat%col(9) = 5; spmat%val(9) = 35.0d0
    ! Fourth row
    spmat%row(10) = 4; spmat%col(10) = 4; spmat%val(10) = 440.0d0
    spmat%row(11) = 4; spmat%col(11) = 6; spmat%val(11) = 46.0d0    
    ! Fifth row
    spmat%row(12) = 5; spmat%col(12) = 5; spmat%val(12) = 550.0d0
    ! Sixth row
    spmat%row(13) = 6; spmat%col(13) = 6; spmat%val(13) = 660.0d0  

    spmat%cum(1) = 1
    spmat%cum(2) = 5    
    spmat%cum(3) = 8
    spmat%cum(4) = 10
    spmat%cum(5) = 12
    spmat%cum(6) = 13
    spmat%nterms = 13

    write(*,*)"Data inserted!"
    !call assemble_and_print(spmat%nrows,spmat%nterms,spmat%row, &
    !spmat%col, spmat%val)
    b(1) = 0.0d0; b(2) = 1.0d0; b(3) = 0.0d0
    b(4) = 2.0d0; b(5) = 3.0d0; b(6) = 5.0d0

    ! Expected solution [-0.00227325,  0.00353409, -0.00057191,  0.00365387,  0.0053923 ,
    !    0.0073762 ]
    ! More preciselly
    ! x[0] = -0.0022732533096921134
    ! x[1] = 0.0035340896955489243
    ! x[2] = -0.0005719103009512305
    ! x[3] = 0.003653868287411465
    ! x[4] = 0.005392297123254459
    ! x[5] = 0.007376203199597191

    call solve_system_of_equation(spmat, b, x)

    write(*,*)"Solution"
    do i=1,6
        write(*,'(a,i2,a,f18.15)')'x(',i,') = ',x(i)
    enddo

    call dot_matrix_vector(spmat,x,Ax)

    diff = Ax - b
    write(*,*)"          Ax                        b                      Ax-b"
    do i=1,6
        write(*,*)Ax(i),b(i),diff(i)
    enddo

end program test_sparse