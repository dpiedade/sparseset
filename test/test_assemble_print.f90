program test_sparse
    use sparse
    implicit none

    type(sparse_matrix)::spmat

    call prepare_to_use(spmat,6,13)

    ! Here we only want to test the method to transform the sparseSET matrix
    ! to be sent to superLU

    ! First row
    spmat%row(1) = 1; spmat%col(1) = 1; spmat%val(1) = 11.0d0
    spmat%row(2) = 1; spmat%col(2) = 4; spmat%val(2) = 14.0d0
    spmat%row(3) = 1; spmat%col(3) = 5; spmat%val(3) = 15.0d0
    spmat%row(4) = 1; spmat%col(4) = 6; spmat%val(4) = 16.0d0    
    ! Second row
    spmat%row(5) = 2; spmat%col(5) = 2; spmat%val(5) = 22.0d0
    spmat%row(6) = 2; spmat%col(6) = 4; spmat%val(6) = 24.0d0
    spmat%row(7) = 2; spmat%col(7) = 5; spmat%val(7) = 25.0d0
    ! Third row
    spmat%row(8) = 3; spmat%col(8) = 3; spmat%val(8) = 33.0d0
    spmat%row(9) = 3; spmat%col(9) = 5; spmat%val(9) = 35.0d0
    ! Fourth row
    spmat%row(10) = 4; spmat%col(10) = 4; spmat%val(10) = 44.0d0
    spmat%row(11) = 4; spmat%col(11) = 6; spmat%val(11) = 46.0d0    
    ! Fifth row
    spmat%row(12) = 5; spmat%col(12) = 5; spmat%val(12) = 55.0d0
    ! Sixth row
    spmat%row(13) = 6; spmat%col(13) = 6; spmat%val(13) = 66.0d0  

    spmat%cum(1) = 1
    spmat%cum(2) = 5    
    spmat%cum(3) = 8
    spmat%cum(4) = 10
    spmat%cum(5) = 12
    spmat%cum(6) = 13
    spmat%nterms = 13

    write(*,*)"Data inserted!"
    call assemble_and_print(spmat%nrows,spmat%nterms,spmat%row, &
    spmat%col, spmat%val)

end program test_sparse