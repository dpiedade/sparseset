## Compiling superLU

For compiling superLU (better saying, the files available in this project),
one must enter the *superlu_double* folder and run the Makefile inside it.
For doing this, just enter the folder using the terminal (bash) and
type `make`.

You will also need some blas functions in order to make __superLU__
work. To compile the need version of __BLAS__, enter the *double_cblas*
folder and type `make`.

If, instead of using WSL or Linux, you are using *mingw* ou *mingw64*,
in windows os, use the following command instead of `make`:
```
mingw32-make -f Makefile.mingw64.win
```

For both cases, the library will be compiled and copied to the *lib* folder
(in this project). You can find them inside this folder (*libsuperlu.a*
and *libblasslu.a*).

[Back to README](../README.md)
