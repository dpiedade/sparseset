## Compiling sparseSET

All subroutines and functions in __sparseSET__ are inside __sparse__ module. In 
order to use them it is necessary to compile _sparse_set.f90_ in your project.
Also it is necessary to include the command `use sparse` in the file(s) you use
__sparseSET__ functions and subroutines (remember that the *use* statements must
precede the __implicit none__ command in a fortran file).

In addition to _sparse_set.f90_ file it is also necessary to use __superLU__ 
library files (*libsuperlu.a* and *libblasslu.a*) during the compilation.

__Example__:

A project containg just one file named __project01.f90__:
(consider that *libsuperlu.a* and *libblasslu.a* are in the same folder)

 ```
 gfortran sparse_set.f90 project01.f90 -o project.out -L. -lsuperlu -lblasslu
 ```

 In the command above, `-L` indicate the folder in which the compiler will find
 the library files; the dot after -L indicates that this folder is the current
 folder. The `-l` indicates the name of the library file to include, 
 suppressing the *lib* in the beginning and the (*.a*) in the end of the file
 name, that must be informed after it.

 For projects with more files, it is straight foward to adapt the compiler
 command. 

[Back to README](../README.md)