## Using sparseSET

The main purpose of __sparseSET__ is to provide functions and subroutines to
easily assemble sparse matrices efficiently. The sparse matrix is stored in 
a fortran *user defined data type* named __sparse_matrix__. Therefore,
you need to define at least one variable of this type in your code. In other
words, your sparse matrices are stored in *sparse_matrix* variables.

```fortran
    type(sparse_matrix)::spmat ! spmat -> variable in which data will be stored
```
Of course, it is necessary to include the library first, by including the 
`use sparse` line in the beginning of the file.

Before starting to build the sparse matrix (which is done by adding local
matrices to the sparse matrix) it is necessary to *'allocate'* space for 
storing these terms. For undestanding how to compute the necessary *'space'*
it is important to undestand how the `sparse_matrix` store them.

Each time the user adds a local matrix to the sparse matrix by calling the 
`add_matrix` __subroutine__, the __upper triangular matrix__ (__sparseSET__ 
only works for symmetric matrix yet) is copied to one-dimensional arrays 
*inside* the `sparse_matrix`. At this stage of the process, terms are only 
copied there (if the user adds the same term __n__ times, there will be
 __n__ copies of this terms in these arrays). These __arrays__ are named:

* __row__ - __integer__ value indicanting the row index of a given value;
* __col__ - __integer__ value indicating the column index of a given value;
* __val__ - __double precision__ (__real(8)__) value storing the value of the
term in __row__ and __col__.

Of course, depending on the local matrix size, an specific number of terms in 
these vector are required (positions used to store it):

* __3x3__ matrix - __6__  positions
* __4x4__ matrix - __10__ positions
* __5x5__ matrix - __15__ positions

* __nxn__ matrix - __(n*n+n)/2__ positions

Therefore, the user must account the number of terms in all the local matrices
to *allocate* the arrays in the sparse matrix. In fact, this is the __minimum__
size for these arrays; you can allocate any value __greater__ than these if you
have enough memory.

 
>**Some users prefer considering the whole local matrix size for evaluating the 
>necessary sparse matrix space. If you have enough memory there is no problem 
>in doing so.**


Let's take one example:

* **10 4x4** matrices
* **20 6x6** matrices
* **4 12x12** matrices

Using the *'exact'* approach to compute the number of terms (_nterms_):

nterms = 10 * (4 * 4 + 4)/2 + 20 * (6 * 6 + 6)/2 + 4 * (12 * 12 + 12)/2
nterms = 100 + 420 + 312 = 832

Using the *'simplified'* approach to compute the number of terms (_nterms_):

nterms = 10 * (4 * 4) + 20 * (6 * 6) + 4 * (12 * 12)
nterms = 160 + 720 + 576 = 1456

In summary, the __minimum__ necessary size for __row__, __col__ and __val__
is 832 *terms*, so the user can use 1456 or any other value greater than 832
for allocating the sparse matrix.

Let's supose that for this example, the number os rows/columns of the system of
equations in __80__ (number of __unknowns__, number of __variables__ or number
of __degrees of freedom__). In this case, before starting to **'push'** values
to the __sparse matrix__ *spmat* one must call __prepare_to_use__:

``` fortran
    call prepare_to_use(spmat, 80, 832) ! nterms = 832, nrows = 80 
```

After allocating the space, one can start to add the local matrices to the
sparse matrix by using the `add_matrix` subroutine, in which is necessary to
inform the indexes (in the sparse matrix) of the row/columns in which the 
local matrix is going to be added.

Example:

Adding a __4x4__ matrix (__kl_4x4__) in rows/columns *2,4,5,7* of __spmat__:

```fortran
   indexes(1) = 2; indexes(2) = 4
   indexes(3) = 5; indexes(4) = 7
   call add_matrix(spmat, kl_4x4, indexes, 4) ! 4 - number of rows/cols of kl
```
In the gerenal situation, after calling add_matrix several times, there will be
several different terms regarding the same position of the sparse matrix (same 
__row__ and __column__), which is not suitable for the sparse solver. The user 
must then call the subroutine to identify these terms and sum them up such that 
each term is unique, in a format that the routine to solve the sparse linear 
system of equation can be called.

For doing this, it is necessary to call `assemble_sparse_matrix`:

```fortran
    call assemble_sparse_matrix(spmat, timeit=.false.)
    ! timeit - boolean; .true. for priting information regarding the time
    ! to assemble the sparse matrix; .false. for no printing 
```

Several methods can be used for applying the boundary conditions. Let's 
consider some situations faced while enforcing boundary conditions and
show how to use other __sparseSET__ functions and subroutines for these
tasks.

__IMPORTANT__: __These methods are supposed to be called after calling__
 `assemble_sparse_matrix`__!__

Setting the value of all terms in a given **row** and **column** as __ZERO__
and them setting the value __ONE__ in the corresponding diagonal term is a 
commonly used strategy to eliminate an equation from the system of equation.
For doing so in the __n<sup>th</sup>__ row/column:

```fortran
    call set_value_to_row(spmat, n, 0.0d0)
    call set_value_to_col(spmat, n, 0.0d0)
    call set_value_in_term(spmat, n, n, 1.0d0)
    ! It is important to set 1.0 after setting zero to the row/col
```

If the associated variable (__degree of freedom__) is set to zero, it is
necessary just to set the __n<sup>th</sup>__ right-hand vector term as zero. 

If the associated variable is set to a value different than zero (__DD__, for 
instance), before *'zeroing'* the __row__/__column__, it is necessary to 
subtract its values multiplied by __DD__ from the right-hand vector. For 
doing the subroutines `get_row` and `get_col` can be used.

In fact, many other situations can arise, and in general the user will find
subroutines and functions to get or modify the necessary terms in 
__sparseSET__. The functions are commented in the source code and we strongly
believe that any user can learn how to use them just reading the comments
in the source code.

Finally, for solving the system of equation, there are two options:

* Using the direct solver (__superLU__)

```fortran
    call solve_system_of_equation(spmat, b, x)
    ! b - right-hand vector
    ! x - solution vector
```

* Using the iterative solver (Conjugate Gradient)

```fortran
    call solve_system_of_equation_cg(spmat, b, x)
    ! b - right-hand vector
    ! x - solution vector
```

If the same *sparse_matrix* variable is going to be *'zeroed'* (for instance,
for a new time step in an iterative computation) it is possible to reset all 
values without deallocating the vectors inside the *sparse_matrix() using the
subroutine `clear_data`.

If the intent is to deallocate it, just use the subroutine 
`deallocate_sparse_matrix`.

Probably one of the easiest ways to learn how to use a library is to read a 
simple example. One can find an example project in the __example__ folder of
this repository.

[Back to README](../README.md)