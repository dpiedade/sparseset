# sparseSET - A library for assembling sparse matrices

This library was written to help students assembling sparse matrices; our
general use is in Finite Element Methods, even though it can be used for other
methods. In the previous release, the _solve_system_of_equation_ subroutine used
the __HSL__ direct sparse solver __MA67__, but we could not share the solver due to
license issues. In this version, we use the __superLU__ solver, that is released
under the BSD license, which allows sharing the source code. In fact, here one
will find only part of the files from a version of __superLU__. Some explanations
on why we've adopted this approach:

* Our library (__sparseSET__) only works with double precision (real) values, so we
have only used the files for this data type;
* The sequential version of __superLU__ was used because we wanted the simplest 
version of the solver;
* The files were rearranged and new Makefiles were created to meet our needs
(we wanted to use the __GNU make__ for compiling the library, and it appeared that the
makefiles included were not working anymore - apparenty they are currently 
using __cMake__ instead).

Probably there are several files from __superLU__ that are included here and are not
indeed necessary for the library to work in our use cases. We hope we change this
in the future. 

__Authors *1 ::__ Dorival Piedade Neto, Rodrigo Ribeiro Paccola

__Current version:__ 1.0.0 (November 2020)

__License *1 :__ BSD license (see License.txt)

__NOTE *1__: for _sparse_set.f90_, _fortran_interface.c_ and any other file 
from __sparseSET__ (please observe that the files included in 
__superlu_double__ and  __double_cblas__ directories are part of the 
__superLU__ library, therefore under the license the library was released)


## Using sparseSET

1. First you need to compile __superLU__ library: 
[Compiling the superLU library](doc/superLU.md)

2. Compiling your code with __sparseSET__: 
[Compiling sparseSET](doc/compiling_sparseSET.md)

3. Learn how to use the subroutines from __sparseSET__:
[Using sparseSET](doc/using_sparseSET.md)